// @ts-check
var canvas;
var context;
var image;

var mouseX;
var mouseY;

var colorPurple = "#cb3594";
var colorGreen = "#659b41";
var colorYellow = "#ffcf33";
var colorRed = "#ff0000";
var colorEraser = "#ffffff";

var curColor = colorGreen;
var curTool = 'marker'

var imageHeight;
var imageWidth;
var imageLeft;
var imageTop;

var paint = false;

function prepareCanvas(){
    image = document.getElementById("source_image");
    imageHeight = image.height;
    imageWidth = image.width;
    imageLeft = image.offsetLeft;
    imageTop = image.offsetTop;
    
    canvas = document.getElementById('canvas');
    setCanvasWidthHeight();
    
    context = canvas.getContext("2d");

    //Mouse events
    canvas.addEventListener("mousedown", function(e){
        mouseX = e.pageX - this.offsetLeft;
        mouseY = e.pageY - this.offsetTop;
    
        paint = true;
    });
    
    canvas.addEventListener("mousemove", function(e){
        if(paint){
            addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
        }
    });
    
    canvas.addEventListener("mouseup", function(e){
        paint = false;
    });
    
    canvas.addEventListener("mouseleave", function(e){
        paint = false;
    });

    //Touch events
    canvas.addEventListener("touchstart", handleStart, false);
    
    canvas.addEventListener("touchmove", handleMove, false);
    
    canvas.addEventListener("touchend", handleEnd, false);
    
    canvas.addEventListener("touchcancel", handleCancel, false);

    var download_button = document.getElementById('btn-download');
    download_button.addEventListener('click', function (e) {
        context.globalCompositeOperation = "destination-over";
        context.drawImage(image, imageLeft * 2, imageTop * 2, imageWidth * 2, imageHeight * 2);

        var dataURL = canvas.toDataURL('image/png');
        download_button.href = dataURL;
         
        clearCanvas();
    });

    var reset_button = document.getElementById('btn-reset');
    reset_button.addEventListener('click', function (e){
        clearCanvas();
    });

    window.addEventListener("orientationchange", function() {                   
        if (window.matchMedia("(orientation: portrait)").matches) {
          setCanvasWidthHeight();
         }
        if (window.matchMedia("(orientation: landscape)").matches) {
          setCanvasWidthHeight();
         }
    }, false);
}

function setCanvasWidthHeight(){
    canvas.height = window.innerHeight;
    canvas.width = window.innerWidth;
}

function clearCanvas(){
    context.clearRect(0, 0, context.canvas.width, context.canvas.height);
}

function downloadImage(){
    context.globalCompositeOperation = "destination-over";
    context.drawImage(image, 1, 1, imageWidth, imageHeight);

    var dataURL = canvas.toDataURL('image/png');
    
    document.write('<img src="'+dataURL+'"/>');
    clearCanvas();
}

function setColor(color){
    setTool('marker');

    switch(color){
        case 'purple': 
            curColor = colorPurple;
            break;
        case 'red': 
            curColor = colorRed;
            break;
        case 'green': 
            curColor = colorGreen;
            break;
        case 'yellow':
            curColor = colorYellow;
            break;
        case 'eraser':
            curColor = colorEraser;
            break;
    }
}

function setTool(tool){
    switch(tool){
        case 'marker':
            curTool = 'marker';
            break;
        case 'eraser':
            curTool = 'eraser';
            break;
    }
}

function addClick(x, y){
    if(curTool != 'eraser') {
        context.lineWidth = 5;
        context.globalCompositeOperation="source-over";
    } else {
        context.lineWidth = 8;
        context.globalCompositeOperation="destination-out";
    }

    context.strokeStyle = curColor;
    context.lineCap = "round";

    context.beginPath();
    context.moveTo(mouseX, mouseY);
    context.lineTo(x,y);
    context.stroke();
    context.closePath();

    mouseX = x;
    mouseY = y;
}

//Touch event handlers
function handleStart(evt) {
    evt.preventDefault();
    var touches = evt.changedTouches[0];
    paint = true;      
    mouseX = touches.pageX
    mouseY = touches.pageY;
}

function handleMove(evt) {  
    var touches = evt.changedTouches[0];

    addClick(touches.pageX, touches.pageY);
}

function handleEnd(evt) {
    evt.preventDefault();
    paint = false;
}

function handleCancel(evt) {
    paint = false;
}